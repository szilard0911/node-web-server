const express = require('express')
const hbs = require('hbs')
const fs = require('fs')
// enviroment variables
const port = process.env.PORT || 8080
var app = express()

//register partial blocks for hbs templates
hbs.registerPartials(__dirname + '/views/partials')
app.set('view engine', 'hbs')


/**
 * Middlewares (app.use(...))
 */

// register (your own) express  middleware
// req - everything that comes from the client http://expressjs.com/en/4x/api.html#req
// res - everything we send to the client http://expressjs.com/en/4x/api.html#res
app.use((req, res, next) => {
    var now = new Date().toString()
    var log = `${now}: ${req.method} ${req.url}`
    fs.appendFile('server.log', log + '\n', (err) => {
        console.log('Unable to append to server.log.')
    })
    console.log(`${now}: ${req.method} ${req.url}`)
    //calling next is required to jump out from the middlewere
    next();
})

/**
 * next nélkül ebben benneragad a middleware-ben és megnyitja a maintenance page-et minden-re. 
 * Ez azért lehet jó mert amíg updatelünk ez minden hívást kilő. kivéve a public-ban lévő static pageket mert az a middleware előbb lett definiálva.
 * (már nem ott van szóval a public pagek sem nyílnak meg)
 * comment out to activate maintenance mode
 */ 
// app.use((req, res, next) => {
//     res.render('maintenance.hbs')
// })

// register public directory for static pages files and etc.
app.use(express.static(__dirname + '/public'))


/**
 * Template Helpers
 */

// register helper functions for the templates - getCurrent year is callable from the views or partials
hbs.registerHelper('getCurrentYear', () => {
    return new Date().getFullYear()
})

hbs.registerHelper('screamIt', (text) => {
    return text.toUpperCase()
})


/**
 * Routers
 */

app.get('/', (req, res) => {
    //res.send('<h1>Hello express</h1>')
    // send json /the content type will be application/json/
    // res.send({
    //     name: 'Szilárd',
    //     likes: [
    //         'music',
    //         'csgo',
    //         'parties'
    //     ]
    // })
    res.render('home.hbs', {
        pageTitle: 'Home Page',
        welcomeMessage: 'Welcome to my website!'
    })
})

app.get('/about', (req, res) => {
    res.render('about.hbs', {
        pageTitle: 'About Page',
    })
})
app.get('/projects', (req, res) => {
    res.render('projects.hbs', {
        pageTitle: 'Projects Page',
    })
})

app.get('/bad', (req, res) => {
    res.send({
        errrorMessage: 'unable to handle request',
    })
})

/**
 * Start Server
 */

app.listen(port, () => {
    console.log('Server is up on 8080 port. http://localhost:' + port)
})